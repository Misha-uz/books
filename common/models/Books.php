<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "books".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $author
 * @property string|null $genre
 * @property int|null $number_of_pages
 * @property int|null $view
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class Books extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'author', 'genre','number_of_pages'],'required'],
            [[  'created_at', 'updated_at'], 'integer'],
            [['name', 'author', 'genre'], 'string', 'max' => 255],
            [['number_of_pages'],'integer','min'=>1,'max'=>999],
            [['view'],'boolean']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'название',
            'author' => 'автор',
            'genre' => 'жанр',
            'number_of_pages' => 'количество страниц',
            'view' => 'показать книгу',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
