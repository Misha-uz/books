<?php

/* @var $this yii\web\View */
/* @var  $books common\models\Books */
$this->title = 'Книги';
?>
<?php
    if (count($books) == 0) {
        echo '<h1 class="text-danger text-center">Нет информации</h1>';
    }
  else{
?>
<h1 class="text-center">Список книг</h1>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">название</th>
        <th scope="col">автор</th>
        <th scope="col">жанр</th>
        <th scope="col">количество страниц</th>
    </tr>
    </thead>
    <tbody>
    <?php $cnt=1; foreach ($books  as $book):?>
    <tr>
        <th scope="row"><?=$cnt++?></th>
        <td><?=$book->name?></td>
        <td><?=$book->author?></td>
        <td><?=$book->genre?></td>
        <td><?=$book->number_of_pages?></td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?php } ?>